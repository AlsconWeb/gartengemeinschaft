jQuery(document).ready(function($) {
    "use strict";

    if($('html').attr('lang') == 'de-DE') {
        if($('ul.mvp-soc-mob-list').length > 0) {
            function changeSocText() {
                $('li.mvp-soc-mob-fb span.mvp-soc-mob-fb').text('Teilen');
                $('li.mvp-soc-mob-twit span.mvp-soc-mob-fb').text('Twittern');
            }
            setTimeout(changeSocText, 500);
        }
    }
	$('.mvp-fly-but-click').click(function(){
		$('body').toggleClass('open-menu');
	})
});
