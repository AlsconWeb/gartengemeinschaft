<?php
add_action( 'wp_enqueue_scripts', 'zox_news_child_theme_enqueue_styles' );
function zox_news_child_theme_enqueue_styles() {

    $parent_style = 'mvp-custom-style';
    wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '', true);
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

/**
 * Set up Zox News Child Theme's textdomain.
 *
 * Declare textdomain for this child theme.
 * Translations can be added to the /languages/ directory.
 */
function zox_news_child__theme_setup() {
    load_child_theme_textdomain( 'zox-news-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'zox_news_child__theme_setup' );

/**
* Add ACF Option page and subpages to Wordpress dashboard
*/
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Child Theme General Settings',
        'menu_title'	=> 'Child Theme Settings',
        'menu_slug' 	=> 'zox-news-child-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Vermoegen Header Settings',
        'menu_title'	=> 'Vermoegen Header',
        'parent_slug'	=> 'zox-news-child-general-settings',
    ));



    acf_add_options_sub_page(array(
        'page_title' 	=> 'Vermoegen Content Settings',
        'menu_title'	=> 'Vermoegen Content',
        'parent_slug'	=> 'zox-news-child-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Vermoegen Footer Settings',
        'menu_title'	=> 'Vermoegen Footer',
        'parent_slug'	=> 'zox-news-child-general-settings',
    ));
}

/**
 * Output easy-to-read numbers
 */
function vermoegen_nice_number($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));

    // is this a number?
    if(!is_numeric($n)) return false;

    // now filter it;
    if($n>=1000000000000) return str_replace('.',',', strval(round(($n/1000000000000),1))).' '.__('Trillion', 'zox-news-child');
    else if($n>=1000000000) return str_replace('.',',', strval(round(($n/1000000000),1))).' '.__('Billion', 'zox-news-child');
    else if($n>=1000000) return str_replace('.',',', strval(round(($n/1000000),1))).' '.__('Million', 'zox-news-child');
    else if($n>1000) { if(round(($n/1000),1) == 1000) { return round(($n/1000),3); } else { return round(($n/1000),1).''.__('.000', 'zox-news-child'); } }

    return number_format($n);
}

/**
 * Filter the single_template with our custom function
 */
add_filter('single_template', 'vermoegen_single_template');

/**
 * Single template function which will choose our template
 */
function vermoegen_single_template($single) {
    global $wp_query, $post;

    $path = get_stylesheet_directory() . '/single/';
    /**
     * Checks for single template by category
     * Check by category slug and ID
     */
    foreach((array)get_the_category() as $cat) :

        if(file_exists($path . 'single-cat-' . $cat->slug . '.php'))
            return $path . 'single-cat-' . $cat->slug . '.php';

        elseif(file_exists($path . 'single-cat-' . $cat->term_id . '.php'))
            return $path . 'single-cat-' . $cat->term_id . '.php';
        else return get_template_directory() . '/single.php';
    endforeach;
}

//* Shortcode to display the current year in WordPress - shortcode: [jahr]
add_shortcode( 'jahr' , 'current_year' );
function current_year() { $year = date("Y"); return "$year"; }

//* Shortcode to display the current month in WordPress - shortcode: [monat]
add_shortcode( 'monat' , 'current_month' );
function current_month() { $month = date("m"); return "$month"; }
add_filter( 'the_title', 'do_shortcode' ); // activate shortcode in WP Title
add_filter( 'wpseo_title', 'do_shortcode' ); // activate shortcode in Yoast Title
add_filter( 'wpseo_metadesc', 'do_shortcode' ); // activate shortcode in Yoast Meta Description
add_filter( 'wpseo_twitter_description', 'do_shortcode' ); // activate shortcode in Yoast Meta Description

/*Remove comment form fields*/
add_filter('comment_form_default_fields', 'v_comment_unset_fields');
function v_comment_unset_fields($fields){
    if(isset($fields['url'])) {
        unset($fields['url']);
    }
    if(isset($fields['email'])) {
        unset($fields['email']);
    }
    return $fields;
}


/////////////////////////////////////
// Auto Load Posts
/////////////////////////////////////

$alp_side = get_option('mvp_alp'); if ($alp_side == "true") {
    if (isset($alp_side)) {
        function getPostHTML($post, $current = false)
        {
            ob_start();
            ?>
            <div class="alp-related-post post-<?php echo esc_html($post->ID); ?> <?php echo (esc_html($current) ? 'current' : ''); ?>" data-id="<?php echo esc_html($post->ID); ?>" data-document-title="">
                <?php
                $postThumbnailUrl = get_the_post_thumbnail_url($post->ID, 'thumbnail');
                if($postThumbnailUrl)
                {
                    ?>

                    <?php
                }
                ?>
                <div class="post-details">
                    <p class="post-meta">
                        <?php
                        $postCategories = get_the_category($post->ID);
                        if($postCategories)
                        {
                            foreach($postCategories as $postCategory)
                            {
                                ?>
                                <a class="post-category" href="<?php echo get_category_link($postCategory->term_id); ?>"><?php echo esc_html($postCategory->name); ?></a>
                                <?php
                            }
                        }
                        ?>
                    </p>
                    <a class="post-title" href="<?php echo get_permalink($post->ID); ?>"><?php echo html_entity_decode($post->post_title); ?></a>
                </div>
                <?php $socialbox = get_option('mvp_social_box'); if ($socialbox == "true") { ?>
                    <div class="mvp-alp-soc-wrap">
                        <ul class="mvp-alp-soc-list">
                            <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink($post->ID);?>&amp;t=<?php the_title_attribute(array('post'=>$post->ID)); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="<?php esc_html_e( 'Share on Facebook', 'zox-news-child' ); ?>">
                                <li class="mvp-alp-soc-fb"><span class="fa fa-facebook"></span></li>
                            </a>
                            <a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title_attribute(array('post'=>$post->ID)); ?> &amp;url=<?php the_permalink($post->ID) ?>', 'twitterShare', 'width=626,height=436'); return false;" title="<?php esc_html_e( 'Tweet This Post', 'zox-news-child' ); ?>">
                                <li class="mvp-alp-soc-twit"><span class="fa fa-twitter"></span></li>
                            </a>
                            <a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink($post->ID);?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'mvp-large-thumb' ); echo esc_url($thumb['0']); ?>&amp;description=<?php the_title_attribute(array('post'=>$post->ID)); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="<?php esc_html_e( 'Pin This Post', 'zox-news-child' ); ?>">
                                <li class="mvp-alp-soc-pin"><span class="fa fa-pinterest-p"></span></li>
                            </a>
                            <a href="mailto:?subject=<?php the_title_attribute(array('post'=>$post->ID)); ?>&amp;BODY=<?php esc_html_e( 'I found this article interesting and thought of sharing it with you. Check it out:', 'zox-news-child' ); ?> <?php the_permalink($post->ID); ?>">
                                <li class="mvp-alp-soc-com"><span class="fa fa-envelope"></span></li>
                            </a>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <?php
            return ob_get_clean();
        }
    } }

    /////////////////////////////////////
// Related Posts
/////////////////////////////////////

if ( !function_exists( 'v_mvp_RelatedPosts' ) ) {
    function v_mvp_RelatedPosts() {
        global $post;
        $orig_post = $post;
        $mvp_related_num = esc_html(get_option('mvp_related_num'));
	    $post_tag = wp_get_post_terms($post->ID, 'post_tag', ['fields'=>'ids']);
	    $term = get_term( $post_tag[0], 'post_tag' );

	    if ( ! empty( $post_tag ) && $term->count > 1 ) {
		    $arg = [
			    'ignore_sticky_posts' => 1,
			    'posts_per_page'      => $mvp_related_num,
			    'post__not_in'        => [ $orig_post->ID ],
			    'order'               => 'DESC',
			    'tag__in'             => $post_tag,
			    'post_status'         => 'publish',
		    ];
	    } else {
		    $arg = [
			    'posts_per_page'      => '6',
			    'ignore_sticky_posts' => 1,
			    'post__not_in'        => [ $post->ID ],
			    'orderby'             => 'rand',
			    'order'               => 'DESC',
		    ];
	    }
        
        $related = new WP_Query($arg);
        

        if( $related->have_posts() ) {
            echo '<ul class="mvp-related-posts-list left related">';
            while( $related->have_posts() ) {
            	$related->the_post();
            	$id = get_the_ID();
            	?>
                    <a href="<?php the_permalink(); ?>" rel="bookmark">
                    <li>
                        <?php if (  has_post_thumbnail($id)  ) { ?>
                            <div class="mvp-related-img left relative">
                                <?php the_post_thumbnail('mvp-mid-thumb', array( 'class' => 'mvp-reg-img' )); ?>
                                <?php the_post_thumbnail('mvp-small-thumb', array( 'class' => 'mvp-mob-img' )); ?>
                                <?php if ( has_post_format( 'video', $id )) { ?>
                                    <div class="mvp-vid-box-wrap mvp-vid-box-mid mvp-vid-marg">
                                        <i class="fa fa-2 fa-play" aria-hidden="true"></i>
                                    </div><!--mvp-vid-box-wrap-->
                                <?php } else if ( has_post_format( 'gallery', $id )) { ?>
                                    <div class="mvp-vid-box-wrap mvp-vid-box-mid">
                                        <i class="fa fa-2 fa-camera" aria-hidden="true"></i>
                                    </div><!--mvp-vid-box-wrap-->
                                <?php } ?>
                            </div><!--mvp-related-img-->
                        <?php } ?>
                        <div class="mvp-related-text left relative">
                            <p><?php the_title(); ?></p>
                        </div><!--mvp-related-text-->
                    </li>
                    </a>
            <?php }
            echo '</ul>';
//            $post = $orig_post;
            wp_reset_postdata();
        }
    }
}

/*
 * Custom exceprt
 */
function custom_excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

add_filter( 'wpseo_schema_webpage', 'date_remove_webpage' );

/**
 * Change @type of Webpage Schema data.
 *
 * @param array $data Schema.org Webpage data array.
 *
 * @return array $data Schema.org Webpage data array.
 */
function date_remove_webpage( $data ) {
    if ( is_singular() ) {
        $data['datePublished'] = '';
        $data['dateModified']  = '';
    }

    return $data;
}

function mvp_comment_child( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'comment' :
            ?>
            <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
            <div class="comment-wrapper" id="comment-<?php comment_ID(); ?>">
                <div class="comment-inner">
                    <div class="comment-avatar">
                        <?php echo get_avatar( $comment, 46 ); ?>
                    </div>
                    <div class="commentmeta">
                        <p class="comment-meta-1">
                            <?php printf( esc_html__( '%s ', 'zox-news-child'), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
                        </p>
                        <p class="comment-meta-2">
                            <?php echo get_comment_date(); ?> <?php esc_html_e( 'at', 'zox-news-child'); ?> <?php echo get_comment_time(); ?> <?php esc_html_e( 'Hour', 'zox-news-child'); ?>
                            <?php edit_comment_link( esc_html__( 'Edit', 'zox-news-child'), '(' , ')'); ?>
                        </p>
                    </div>
                    <div class="text">
                        <?php if ( $comment->comment_approved == '0' ) : ?>
                            <p class="waiting_approval"><?php esc_html_e( 'Your comment is awaiting moderation.', 'zox-news-child' ); ?></p>
                        <?php endif; ?>
                        <div class="c">
                            <?php comment_text(); ?>
                        </div>
                    </div><!-- .text  -->
                    <div class="clear"></div>
                    <div class="comment-reply"><span class="reply"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></span></div>
                </div><!-- comment-inner  -->
            </div><!-- comment-wrapper  -->
            <?php
            break;
        case 'pingback'  :
        case 'trackback' :
            ?>
            <li class="post pingback">
            <p><?php esc_html_e( 'Pingback:', 'zox-news-child' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( esc_html__( 'Edit', 'zox-news-child' ), ' ' ); ?></p>
            <?php
            break;
    endswitch;
}
/**
 * Alex L Start
 */
function custom_post_template_css() {
	ob_start();
	include_once get_stylesheet_directory() . '/assets/css/amp.css';
	echo ob_get_clean();
}

add_action( 'amp_post_template_css', 'custom_post_template_css', 11 );


function remove_parent_filters() {
	remove_filter( 'amp_post_template_file', 'mvp_amp_set_custom_template' );
	
	add_theme_support(
		'amp',
		array(
			'nav_menu_dropdown' => array(
				'sub_menu_button_class'        => 'dropdown-toggle',
				'sub_menu_button_toggle_class' => 'toggled-on', // (optional)
				'expand_text'                  => __( 'expand', 'domain' ),
				'collapse_text'                => __( 'collapse', 'domain' ),
			),
		)
	);
}

add_action( 'after_setup_theme', 'remove_parent_filters' );

add_filter( 'amp_post_template_file', 'custom_template_amp', 10, 3 );
function custom_template_amp( $file, $type, $post ) {
	if ( 'single' === $type ) {
		$file = get_stylesheet_directory() . '/amp-single.php';
	}
	
	if('page'=== $type){
		$file = get_stylesheet_directory() . '/amp-page.php';
	}
	
	return $file;
}

function add_nav_sub_menu_buttons( $item_output, $item ) {
	if(!amp_is_request()){
		return $item_output;
	}
    // Skip when the item has no sub-menu.
    if ( ! in_array( 'menu-item-has-children', $item->classes, true ) ) {
	    return $item_output;
    }

    // Obtain the initial expanded state.
    $expanded = in_array( 'current-menu-ancestor', $item->classes, true );

    // Generate a unique state ID.
    static $nav_menu_item_number = 0;
    $nav_menu_item_number++;
    $expanded_state_id = 'navMenuItemExpanded' . $nav_menu_item_number;

    // Create new state for managing storing the whether the sub-menu is expanded.
    $item_output .= sprintf(
	    '<amp-state id="%s"><script type="application/json">%s</script></amp-state>',
	    esc_attr( $expanded_state_id ),
	    wp_json_encode( $expanded )
    );

    /*
     * Create the toggle button which mutates the state and which has class and
     * aria-expanded attributes which react to the state changes.
     */
    $dropdown_button  = '<button';
    $dropdown_class   = 'dropdown-toggle';
    $toggled_class    = 'toggled-on';
    $dropdown_button .= sprintf(
	    ' class="%s" [class]="%s"',
	    esc_attr( $dropdown_class . ( $expanded ? " $toggled_class" : '' ) ),
	    esc_attr( sprintf( "%s + ( $expanded_state_id ? %s : '' )", wp_json_encode( $dropdown_class ), wp_json_encode( " $toggled_class" ) ) )
    );
    $dropdown_button .= sprintf(
	    ' aria-expanded="%s" [aria-expanded]="%s"',
	    esc_attr( wp_json_encode( $expanded ) ),
	    esc_attr( "$expanded_state_id ? 'true' : 'false'" )
    );
    $dropdown_button .= sprintf(
	    ' on="%s"',
	    esc_attr( "tap:AMP.setState( { $expanded_state_id: ! $expanded_state_id } )" )
    );
    $dropdown_button .= '>';

    // Let the screen reader text in the button also update based on the expanded state.
    $dropdown_button .= sprintf(
	    '<span class="screen-reader-text" [text]="%s">%s</span>',
	    esc_attr( sprintf( "$expanded_state_id ? %s : %s", wp_json_encode( __( 'collapse child menu', 'example' ) ), wp_json_encode( __( 'expand child menu', 'example' ) ) ) ),
	    esc_html( $expanded ? __( 'collapse child menu', 'example' ) : __( 'expand child menu', 'example' ) )
    );

    $dropdown_button .= '</button>';

    $item_output .= $dropdown_button;
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'add_nav_sub_menu_buttons', 10, 2 );

add_filter( 'widget_categories_args', 'deleteTitleInCategoryList', 50, 1 );
function deleteTitleInCategoryList($args){
	$args['use_desc_for_title'] = false;

	return $args;
}

add_filter('wpseo_robots', 'fixPagedNoIndex', 10, 2);
function fixPagedNoIndex($robots, $presentation ){

	$page = get_query_var('paged');
	if($page>1){
		$robots = 'noindex, follow';
		return $robots;
	}

	return $robots;
}

/**
 * Alex L End
 */